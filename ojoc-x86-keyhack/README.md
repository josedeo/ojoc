# Key retrieval hack #

This is the successor to ojoc-mips-keyhack, now based on the x86
version of the `libhmac.so`.

The decryption method (`GenerateEncryptionKey()` and `decode()`)
have been contributed by @jangarcia.

The decompiled source of the `libhmac.so` shows that the exported
`Java_com_jodelapp_v3_..._init()` function builds a key in memory,
which is XORed with the APK signature hash and shuffled.

`ojoc-x86-keyhack` attempts to find the initial key in the binary file by
just feeding the offsets from the decompiled source and the respective
sizes in bytes.

`ojoc-decrypt` reads the key bytes directly from stdin (if they are
extracted by hand or a more advanced decompiler than retdec) and
decrypts the key. It accepts the key as list of `0x` prefixed hex
bytes.

Both programs read their input from stdin and finish reading only
when they encounter EOF (e.g., when redirecting a file to their
input or by CTRL+D on the terminal)
